from selenium.webdriver.common.by import By


class AddProjectPage:

    # Inicjalizacja klasy - przekazanie drivera (przeglądarki)
    def __init__(self, browser):
        self.browser = browser

    def fill_form(self, form_data):
        name_input = self.browser.find_element(By.ID, 'name')
        prefix_input = self.browser.find_element(By.ID, 'prefix')
        description_input = self.browser.find_element(By.ID, 'description')
        button_save = self.browser.find_element(By.NAME, 'save')
        name_input.send_keys(form_data['name'])
        prefix_input.send_keys(form_data['prefix'])
        description_input.send_keys(form_data['description'])
        # button_save.submit()
        button_save.click()
