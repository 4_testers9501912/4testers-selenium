from selenium.webdriver.common.by import By


class ProjectsPage:

    # Inicjalizacja klasy - przekazanie drivera (przeglądarki)
    def __init__(self, browser):
        self.browser = browser

    def go_to_add_project_page(self):
        self.browser.find_element(By.CSS_SELECTOR, '.button_link').click()

    def verify_the_current_page_is_project_page(self):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

    def search_project_by_name(self, project_name):
        search_input = self.browser.find_element(By.ID, 'search')
        search_button = self.browser.find_element(By.ID, 'j_searchButton')
        search_input.send_keys(project_name)
        search_button.click()

    def verify_project_by_name_in_search_results(self, project_name):
        results = self.browser.find_elements(By.CSS_SELECTOR, 'article table tbody tr td')
        # lista tytułów
        list_of_projects = []
        for i in results:
            list_of_projects.append(i.text)
        assert project_name in list_of_projects
