from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

def test_searching_in_google():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony google
    browser.get('https://www.google.pl/')

    # Ustawienie wielkości strony
    browser.set_window_size(1920, 1080)

    # Zaakceptowanie cookie
    cookie_accept = browser.find_element(By.CSS_SELECTOR, '#L2AGLb')
    cookie_accept.click()

    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.ID, 'APjFqb')

    # Asercje że element jest widoczny dla użytkownika
    assert search_input.is_displayed() is True

    # Szukanie 4_testers
    # search_input.send_keys('4testers')
    # search_input.submit()

    # Szukanie 4_testers
    search_input.send_keys('4testers' + Keys.ENTER)

    # czekanie na dociągnięcie elementów na stronie
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h3')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Sprawdzenie że wynik ma tytuł '4_testers – kurs dla testerów oprogramowania #1 w Polsce'
    # lista Seleniumowcyh WebElementów, które reprezentują tytuły stron
    results = browser.find_elements(By.CSS_SELECTOR, 'h3')

    # lista tytułów (stringów)
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    # Asercja
    assert '4_testers – kurs dla testerów oprogramowania #1 w Polsce' in list_of_titles

    # Zamknięcie przeglądarki
    browser.quit()

