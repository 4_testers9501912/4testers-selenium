import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager
from pages.arena.arena_login import ArenaLoginPage
from pages.home import HomePage
from pages.projects import ProjectsPage
from pages.add_project import AddProjectPage
from utils.random_message import generate_random_text


# Zaloguj się do TestArena demo.testarena.pl
@pytest.fixture()
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_should_open_page_with_adding_a_new_project(browser):
    # Otwórz Admin Panel (przycisk w prawym górnym rogu)
    home_page = HomePage(browser)
    home_page.open_admin_panel()
    projects_page = ProjectsPage(browser)
    projects_page.go_to_add_project_page()
    add_project_page = AddProjectPage(browser)
    # Dodaj nowy projekt
    form_data = {
                "name": generate_random_text(10),
                "prefix": generate_random_text(4),
                "description": generate_random_text(50)
    }
    add_project_page.fill_form(form_data)
    # print(form_data["name"])
    # Wejdź do sekcji Projects
    browser.get('http://demo.testarena.pl/administration/projects')
    # Wyszukaj nowo utworzony projekt po nazwie
    projects_page.verify_the_current_page_is_project_page()
    projects_page.search_project_by_name(form_data["name"])
    # time.sleep(5)
    projects_page.verify_project_by_name_in_search_results(form_data["name"])
