import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


def test_post_count():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    browser.get('https://awesome-testing.blogspot.com/')

    # Pobranie listy tytułów
    results = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    # Asercja że lista ma 4 elementy
    assert len(list_of_titles) == 4

    # Zamknięcie przeglądarki
    browser.quit()


def test_post_count_after_search():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    browser.get('https://awesome-testing.blogspot.com/')

    # Inicjalizacja searchbara i przycisku search
    search_input = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, 'input.gsc-search-button')

    # Szukanie
    search_input.send_keys('selenium')
    search_button.click()

    # Czekanie na stronę
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, '.status-msg-body')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 20 elementów
    assert len(titles) == 20

    # Zamknięcie przeglądarki
    browser.quit()


def test_post_count_on_cypress_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    browser.get('https://awesome-testing.blogspot.com/')

    # Inicjalizacja elementu z labelką
    label = browser.find_element(By.CSS_SELECTOR, 'h1 a')

    # Kliknięcie na labelkę
    label.click()

    # Czekanie na stronę
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h1')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Pobranie listy tytułów
    results = browser.find_elements(By.CSS_SELECTOR, 'h1')
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    # Asercja że lista ma 1 element
    assert len(list_of_titles) == 1

    # Zamknięcie przeglądarki
    browser.quit()


    # Funkcja anonimowa, przyjmuje 1 parametr, nie może być ponownie wywołana.
    # Funkcje lambda są często używane w przypadkach, gdy potrzebujemy krótkiej funkcji,
    # która nie będzie używana wielokrotnie. Nie używa się "def", ani "return".
    # wait.until(lambda driver: len(driver.find_elements(By.CSS_SELECTOR, '.post-title')) == 15)


def test_post_count_on_2016_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    browser.get('https://awesome-testing.blogspot.com/')

    # Inicjalizacja elementu z labelką
    label_2016 = browser.find_element(By.LINK_TEXT, '2016')

    # Kliknięcie na labelkę 2016
    label_2016.click()

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, 'h1')

    # Asercja że lista ma 24 elementy
    assert len(titles) == 24

    # Zamknięcie przeglądarki
    browser.quit()