import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from pages.home import HomePage
from pages.search_result import SearchResultPage


@pytest.fixture()
def browser():
    # 1 - wykona się przed każdym testem, który korzysta z tego fixrure'a
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('https://awesome-testing.blogspot.com/')
    # 2 - granica (to zwracamy = przekazujemy do testów)
    yield driver
    # 3 - wykona się po każdym teście, który korzysta z tego fixrure'a
    driver.quit()


def test_post_count(browser):
    home_page = HomePage(browser)
    home_page.verify_post_count(4)


def test_post_count_after_search(browser):
    home_page = HomePage(browser)
    home_page.search_for('selenium')

    search_result_page = SearchResultPage(browser)
    search_result_page.wait_for_load()
    search_result_page.verify_post_count(20)


def test_post_count_on_2016_label(browser):
    home_page = HomePage(browser)
    home_page.click_label('2016')
    search_result_page = SearchResultPage(browser)
    search_result_page.verify_post_count(24)


def test_post_count_on_first_label(browser):
    # Inicjalizacja elementu z labelką
    label = browser.find_element(By.CSS_SELECTOR, 'h1 a')

    # Kliknięcie na labelkę
    label.click()

    # Czekanie na stronę
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, 'h1')
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, 'h1')

    # Asercja że lista ma 1 element
    assert len(titles) == 1

